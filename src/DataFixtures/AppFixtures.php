<?php

namespace App\DataFixtures;

use App\Entity\Customers;
use App\Entity\Products;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $data_products = [
            [
                'name' => 'Indomie Goreng',
                'price' => '3500',
                'description' => 'Merupakan sebuah makanan dari buatan indofood',
                'image' => ''
            ],
            [
                'name' => 'Indomie Rebus',
                'price' => '3000',
                'description' => 'Merupakan sebuah makanan dari buatan indofood',
                'image' => ''
            ],
            [
                'name' => 'Kopi Kapal Api',
                'price' => '2500',
                'description' => 'Merupakan sebuah minuman seduh',
                'image' => ''
            ],
            [
                'name' => 'Ale-ale',
                'price' => '2000',
                'description' => 'Merupakan sebuah minuman gelas',
                'image' => ''
            ],
            [
                'name' => 'Teh Rio',
                'price' => '2000',
                'description' => 'Merupakan sebuah minuman gelas',
                'image' => ''
            ],
        ];

        $data_users = [
            [
                'name' => 'John Doe',
                'email' => 'johndoe@gmail.com',
                'gender' => 'male',
                'phone' => '08123456789',
            ],
            [
                'name' => 'Rahmad Doe',
                'email' => 'rahmaddoe@gmail.com',
                'gender' => 'male',
                'phone' => '08123486789',
            ],
            [
                'name' => 'Rachell Anantasari',
                'email' => 'rachel@gmail.com',
                'gender' => 'female',
                'phone' => '08523456789',
            ],
        ];

        foreach($data_products as $data) {
            $product = new Products();
            $product->setName($data['name']);
            $product->setPrice($data['price']);
            $product->setDescription($data['description']);
            $product->setImage($data['image']);
            $manager->persist($product);
        }

        $date = new DateTime();

        foreach($data_users as $data) {
            $customers = new Customers();
            $customers->setName($data['name']);
            $customers->setEmail($data['email']);
            $customers->setPhone($data['phone']);
            $customers->setGender($data['gender']);
            $customers->setCreatedAt($date);
            $manager->persist($customers);
        }

        $manager->flush();
    }
}
