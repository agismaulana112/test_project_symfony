<?php


namespace App\Controller;

use App\Entity\Customers;
use App\Repository\CustomersRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends AbstractController{
    public function index(CustomersRepository $customersRepository): Response 
    {
        $customers = $customersRepository->findAll();

        return $this->render('customers/index.html.twig', [
            'customers' => $customers
        ]);
    }

    public function create():Response
    {
        return $this->render('customers/create.html.twig', []);
    }

    public function store(CustomersRepository $customersRepository, Request $request):Response
    {
        $data_request = $request->request->all();

        $customers = new Customers();
        $customers->setName($data_request['name']);
        $customers->setEmail($data_request['email']);
        $customers->setPhone($data_request['phone']);
        $customers->setGender($data_request['gender']);
        $customers->setCreatedAt(new DateTime());
        $customersRepository->save($customers, true);

        return $this->redirectToRoute('customers', [], 201);
    }

    public function show($id, CustomersRepository $customersRepository): Response
    {
        $customer = $customersRepository->find($id);
        $orders = $customer->getOrders();

        return $this->render('customers/detail.html.twig', [
            'customer' => $customer,
        ]);
    }

    public function edit($id, CustomersRepository $customersRepository) {
        $customer = $customersRepository->find($id);

        return $this->render('customers/edit.html.twig', [
            'customer' => $customer,
        ]);
    }

    public function update($id, CustomersRepository $customersRepository, Request $request):Response
    {
        $data_request = $request->request->all();
        $customer = $customersRepository->find($id);
        if(!$customer) {
            return $this->redirectToRoute('customers', [], 301);
        }
        $customer->setName($data_request['name']);
        $customer->setEmail($data_request['email']);
        $customer->setGender($data_request['gender']);
        $customer->setPhone($data_request['phone']);

        $customersRepository->save($customer, true);

        return $this->redirectToRoute('customers', [], 201);
    }


    public function delete($id, CustomersRepository $customersRepository) {
        $customer = $customersRepository->find($id);
        if(!$customer)
            return $this->redirectToRoute('customers', [], 301);

        $customersRepository->remove($customer, true);
        return $this->redirectToRoute('customers', [], 301);
    }
}