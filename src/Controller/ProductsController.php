<?php

namespace App\Controller;

use App\Entity\Products;
use App\Repository\ProductsRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends AbstractController
{
    #[Route('/products', name: 'products')]
    public function index(ProductsRepository $productsRepository): Response
    {
        $products = $productsRepository->findAll();

        return $this->render('products/index.html.twig', [
            'products' => $products,
        ]);
    }

    #[Route('/products/create', name: 'products_create')]
    public function create():Response
    {
        return $this->render('products/create.html.twig', []);
    }

    #[Route('products/store', name: 'products_store')]
    public function store(
        Request $request, 
        ProductsRepository $productsRepository,
        FileUploader $uploader
    ): Response
    {
        $data_request = $request->request->all();
        $file = $request->files->get('image');

        $product = new Products();
        $product->setName($data_request['name']);
        $product->setDescription($data_request['description']);
        $product->setPrice($data_request['price']);

        if($file) {
            $uploadDir = $this->getParameter('upload_dir');
            $filename = $file->getClientOriginalName();
            $fileExt = $file->getClientOriginalExtension();
            $get_file = $uploader->upload($uploadDir, $file, $filename, $fileExt);
            $product->setImage($get_file);
        }

        $productsRepository->save($product, true);

        return $this->redirectToRoute('products', [], 201);
    }

    #[Route('products/{id}/edit', name: 'products_edit')]
    public function edit($id, ProductsRepository $productsRepository): Response
    {
        $product = $productsRepository->find($id);

        if(!$product)
            return $this->redirectToRoute('products', [], 301);

        return $this->render('products/edit.html.twig', [
            'product' => $product
        ]);
    }

    #[Route('products/{id}/update', name: 'products_update')]
    public function update(
        $id, 
        Request $request, 
        ProductsRepository $productsRepository,
        FileUploader $uploader
    ): Response
    {
        $data_request = $request->request->all();
        $product = $productsRepository->find($id);

        if(!$product)
            return $this->redirectToRoute('products', [], 301);

        $product->setName($data_request['name']);
        $product->setDescription($data_request['description']);
        $product->setPrice($data_request['price']);

        $file = $request->files->get('image');

        if($file) {
            $uploadDir = $this->getParameter('upload_dir');
            $filename = $file->getClientOriginalName();
            $fileExt = $file->getClientOriginalExtension();
            $get_file = $uploader->upload($uploadDir, $file, $filename, $fileExt);
            $product->setImage($get_file);
        }

        $productsRepository->save($product, true);

        return $this->redirectToRoute('products', [], 201);
    }

    #[Route('products/{id}/delete', name: 'products_delete')]
    public function delete($id, ProductsRepository $productsRepository): Response
    {
        $product = $productsRepository->find($id);

        if(!$product) 
            return $this->redirectToRoute('products', [], 301);

        $productsRepository->remove($product, true);
        return $this->redirectToRoute('products', [], 301);

    }
}
