<?php

namespace App\Controller;

use App\Entity\OrderDetails;
use App\Entity\Orders;
use App\Repository\CustomersRepository;
use App\Repository\OrderDetailsRepository;
use App\Repository\OrdersRepository;
use App\Repository\ProductsRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use function PHPSTORM_META\map;

class OrdersController extends AbstractController
{
    #[Route('/orders', name: 'orders')]
    public function index(OrdersRepository $ordersRepository): Response
    {
        $orders = $ordersRepository->findall();

        return $this->render('orders/index.html.twig', [
            'orders' => $orders,
        ]);
    }

    public function show($id, OrdersRepository $ordersRepository):Response
    {
        $order = $ordersRepository->find($id);

        return $this->render('orders/detail.html.twig', [
            'order' => $order
        ]);
    }

    #[Route('/orders', name: 'orders_create')]
    public function create(
        CustomersRepository $customersRepository,
        ProductsRepository $productsRepository
    ):Response
    {
        $customers = $customersRepository->findall();
        $products = $productsRepository->findall();

        return $this->render('orders/create.html.twig', [
            'customers' => $customers,
            'products' => $products
        ]);
    }

    public function store(
        Request $request,
        OrdersRepository $ordersRepository,
        ProductsRepository $productsRepository,
        CustomersRepository $customersRepository,
        OrderDetailsRepository $orderDetailsRepository
    ): Response
    {
        $data_request = $request->request->all();
        $customers = $customersRepository->find($data_request['customer_id']);

        $order = new Orders();
        $order->setCreatedAt(new DateTime());
        $order->setCustomer($customers);
        $order->setTotal(0);
        $ordersRepository->save($order, true);

        $order_totals = 0;
        foreach($data_request['product_id'] as $key => $product) {
            $product = $productsRepository->find($product);
            $order_details = new OrderDetails();
            $order_details->setProduct($product);
            $order_details->setQuantity($data_request['quantity'][$key]);

            $total = $product->getPrice() * $order_details->getQuantity();

            $order_details->setTotal($total);
            $order_details->setOrders($order);
            $orderDetailsRepository->save($order_details, true);

            $order_totals += $total;
        }

        $order->setCodeOrders('KD-000'.$order->getId());
        $order->setTotal($order_totals);
        $ordersRepository->save($order, true);

        return $this->redirectToRoute('orders', [], 201);
    }

    public function edit(): Response
    {
        return $this->render('orders/edit.html.twig', [
            'order' => ''
        ]);
    }

    public function update(): Response
    {
        return $this->redirectToRoute('orders', [], 201);
    }

    public function delete(
        $id, 
        OrdersRepository $ordersRepository
    ): Response
    {
        $order = $ordersRepository->find($id);

        if(!$order) {
            return $this->redirectToRoute('orders', [], 301);
        }

        $ordersRepository->remove($order, true);

        return $this->redirectToRoute('orders', [], 301);
    }
}